Nuntium SMS gateway module for SMS Framework
--------------------------------------------

Installation
------------

- Install SMS Framework module for Drupal (http://drupal.org/project/smsframework). Copy the ‘smsframework’ folder to the modules folder inside your Drupal installation.
- Install the Nuntium SMS module. Copy the ‘sms_nuntium’ folder to rhe modules folder inside your Drupal installation.
- Log into Drupal as an admin.
- Go to Administer -> Site Building -> Modules.
- You should see the “SMS Framework” and “Nuntium Gateway” modules listed. Enable both and save the configuration.

The Nuntium module uses the Nuntium PHP Api (http://bitbucket.org/instedd/nuntium-api-php). This library depends on curl. Make sure mod_curl is enabled in your PHP configuration.

Configuration
-------------

1. In Drupal (logged in as an admin) go to Administer -> SMS Framework -> Gateway Configuration.
2. You should see “Nuntim” in the listing. Select it and click in “configure”
3. Under “Target URL” you should see an URL like “http://domain-for-my-drupal/sms/nuntium/receiver” copy this URL, we will get back to this form later.
4. Go to http://nuntium.instedd.org and create an account
5. Create a new application
6. Enter a name for the application and a password. 
7. Select “HTTP Post Callback” as the interface.
8. In the interface configuration, paste the URL you copied in step 3 in the Url field, enter a user and a password (user and password can be anything you want, they are credentials to secure your callback).
9. Click in “Create Application”
10. Back in Drupal:
  a. In “Base Nuntium URL” enter http://nuntium.instedd.org
  b. In “Account Name” enter the name of your Nuntium account (the one you used to log in to Nuntium in step 4)
  c. In “Application Name” enter the name of the application you entered in step 6.
  d. In “Application Password” enter the password of the application you entered in step 6.
  e. In “Incoming username” enter the user you entered in step 8.
  f. In “Incoming password” enter the password you entered in step 8.
  g. Save the configuration
11. Now you should be back in the gateway selection, make sure Nuntium is selected and press “Set default gateway”

At this point you should have the SMS Framework module configured to be used with Nuntium. You can continue adding modules to provide more functionality to SMS Framework.
