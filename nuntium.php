<?php
/**
 * Provides access to the Nuntium Public API.
 */
class Nuntium {

  /**
   * Creates an application-authenticated Nuntium API access.
   *
   * @param string $url the URL of the service (normally "http://nuntium.instedd.org")
   * @param string $account the account name
   * @param string $application the application name
   * @param string $password the application's password
   */
  function __construct($url, $account, $application, $password) {
    $this->url = $url;
    $this->account = $account;
    $this->application = $application;
    $this->auth = "$account/$application:$password";
  }

  /**
   * Returns a list of countries known to Nuntium as an array of stdClass with the countries' properties.
   *
   * @throws NuntiumException if something goes wrong
   * @return array an array of stdClass
   */
  function getCountries() {
    return $this->getJson("/api/countries.json");
  }

  /**
   * Returns a country given its iso code as an stdClass with the country's properties.
   *
   * @throws NuntiumException if something goes wrong
   * @return stdClass the country
   */
  function getCountry($iso) {
    return $this->getJson("/api/countries/$iso.json");
  }

  /**
   * Returns a list of carriers known to Nuntium as an array of stdClass with the carriers' properties.
   *
   * @param string $country_id (optional) a country's iso to only return carriers of the country
   * @throws NuntiumException if something goes wrong
   * @return array an array of stdClass
   */
  function getCarriers($country_iso = null) {
    if ($country_iso) {
      return $this->getJson("/api/carriers.json?country_id=$country_iso");
    } else {
      return $this->getJson("/api/carriers.json");
    }
  }


  /**
   * Returns a carrier given its guid as an stdClass with the carrier's properties.
   *
   * @throws NuntiumException if something goes wrong
   * @return stdClass the carrier
   */
  function getCarrier($guid) {
    return $this->getJson("/api/carriers/$guid.json");
  }

  /**
   * Returns the list of channels belonging to the application ot that don't
   * belong to any application, as an array of stdClass with the channels' properties.
   *
   * @throws NuntiumException if something goes wrong
   * @return array an array of stdClass
   */
  function getChannels() {
    return $this->getChannels0("/api/channels.json");
  }

  /**
   * Returns a channel given its name as an stdClass with the channel's properties.
   *
   * @param string $name the name of the channel to retrieve
   * @throws NuntiumException if something goes wrong
   * @return stdClass the channel
   */
  function getChannel($name) {
    $channel = $this->getJson("/api/channels/$name.json");
    $this->readConfiguration($channel);
    return $channel;
  }

  /**
   * Creates a channel.
   *
   * @param array $channel an array containing the channel's properties. The configuration key must be an array of key value pairs.
   * @throws NuntiumException if something goes wrong. You can access specific errors on properties via the getProperties() method of the exception.
   * @return stdClass the created channel
   */
  function createChannel($channel) {
    $this->writeConfiguration($channel);
    $response = $this->post("/api/channels.json", json_encode($channel));
    $channel = json_decode($response);
    $exception = null;

    if ($this->code == 400) {
      $exception = $this->buildChannelException($channel);
    } else {
      $this->readConfiguration($channel);
    }
    $this->close();

    if ($exception) throw $exception;
    return $channel;
  }

  /**
   * Updates a channel.
   *
   * @param array $channel an array containing the channel's properties. The configuration key must be an array of key value pairs.
   * @throws NuntiumException if something goes wrong. You can access specific errors on properties via the getProperties() method of the exception.
   * @return stdClass the updated channel
   */
  function updateChannel($channel) {
    if (!array_key_exists('name', $channel)) {
      throw new NuntiumException("Can't update a channel without a name");
    }

    $this->writeConfiguration($channel);
    $response = $this->put("/api/channels/" . $channel['name'] . ".json", json_encode($channel));
    $channel = json_decode($response);
    $exception = null;

    if ($this->code == 400) {
      $exception = $this->buildChannelException($channel);
    } else {
      $this->readConfiguration($channel);
    }
    $this->close();

    if ($exception) throw $exception;
    return $channel;
  }

  /**
   * Deletes a channel given its name.
   *
   * @param string $name the name of the channel
   * @throws NuntiumException if something goes wrong
   */
  function deleteChannel($name) {
    $this->delete("/api/channels/$name");
    $this->close();
  }

  /**
   * Returns the list of candidate channels when simulating routing the given AO message as an array of stdClass
   * containing the channels' properties.
   *
   * @param array $message an array containing the key-value pairs of the message
   * @throws NuntiumException if something goes wrong
   * @return array an array of stdClass
   */
  function getCandidateChannelsForAO($message) {
    return $this->getChannels0("/api/candidate/channels.json?" . http_build_query($message));
  }

  /**
   * Sends one or many AO messages.
   *
   * @param mixed $messages an array containing the key-value pairs of the message, or an array of the previous explained array
   * @throws NuntiumException if something goes wrong
   * @return stdClass an stdClass containing the id, guid and token properties in the case of a single
   * message, or just token in the case of multiple messages.
   */
  function sendAO($messages) {
    $obj = new stdClass;
    if (array_key_exists(0, $messages)) {
      $this->post("/" . $this->account . "/" . $this->application . "/send_ao.json", json_encode($messages));
    } else {
      $this->get("/" . $this->account . "/" . $this->application . "/send_ao?" . http_build_query($messages));
      $obj->id = $this->extractHeader('X-Nuntium-Id');
      $obj->guid = $this->extractHeader('X-Nuntium-Guid');
    }
    $obj->token = $this->extractHeader('X-Nuntium-Token');
    $this->close();
    return $obj;
  }

  /**
   * Gets AO messages that have the given token.
   *
   * @param string $token a token
   * @throws NuntiumException if something goes wrong
   * @return array an array of stdClass with the messages' properties
   */
  function getAO($token) {
    return $this->getJson("/" . $this->account . "/" . $this->application . "/get_ao.json?token=$token");
  }

  /**
   * Get the custom attributes specified fot a given address.
   *
   * @param string $address an address with a protocol
   * @throws NuntiumException if something goes wrong
   * @return stdClass an stdClass containing the custom attributes
   */
  function getCustomAttributes($address) {
    return $this->getJson("/api/custom_attributes?address=$address");
  }

  /**
   * Sets custom attributes of a given address.
   *
   * @param string $address an address with a protocol
   * @param array $attribtues an array of key-value pairs to set as attributes
   * @throws NuntiumException if something goes wrong
   */
  function setCustomAttributes($address, $attributes) {
    $this->post("/api/custom_attributes?address=$address", json_encode($attributes));
    $this->close();
  }

  private function getChannels0($url) {
    $channels = $this->getJson($url);
    foreach($channels as $channel) {
      $this->readConfiguration($channel);
    }
    return $channels;
  }

  private function buildChannelException($channel) {
    $exception = new NuntiumException($channel->summary);
    $properties = array();
    foreach($channel->properties as $prop) {
      foreach($prop as $key => $value) {
        $properties[$key] = $value;
      }
    }
    $exception->setProperties($properties);
    return $exception;
  }

  private function getJson($url) {
    $response = $this->get($url);
    $response = json_decode($response);
    $this->close();

    return $response;
  }

  private function get($url) {
    $this->init($url);
    return $this->exec();
  }

  private function post($url, $body) {
    $this->init($url);
    curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($this->curl, CURLOPT_POSTFIELDS, $body);
    curl_setopt($this->curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    return $this->exec();
  }

  private function put($url, $body) {
    $this->init($url);
    curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($this->curl, CURLOPT_POSTFIELDS, $body);
    curl_setopt($this->curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    return $this->exec();
  }

  private function delete($url, $body) {
    $this->init($url);
    curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, "DELETE");
    return $this->exec();
  }

  private function init($url) {
    $this->curl = curl_init($this->url . $url);
    curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($this->curl, CURLOPT_HEADER, TRUE);
    curl_setopt($this->curl, CURLOPT_USERPWD, $this->auth);
  }

  private function exec() {
    $response = curl_exec($this->curl);
    if (!$response) throw new NuntiumException("Can't reach " . $this->url);

    $this->code = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);
    if ($this->code != 200 && $this->code != 400) {
      throw new NuntiumException("Received HTTP status code: $this->code");
    }
    list($this->headers, $body) = explode("\r\n\r\n", $response, 2);
    return $body;
  }

  private function extractHeader($name) {
    preg_match("/$name: (.+)/", $this->headers, $matches);
    return $matches[1];
  }

  private function close() {
    curl_close($this->curl);
  }

  private function readConfiguration(&$channel) {
    $config = array();
    foreach($channel->configuration as $obj) {
      $config[$obj->name] = $obj->value;
    }
    $channel->configuration = $config;
  }

  private function writeConfiguration(&$channel) {
    $config = array();
    if (array_key_exists('configuration', $channel)) {
      foreach($channel['configuration'] as $name => $value) {
        $config[] = array('name' => $name, 'value' => $value);
      }
    }
    $channel['configuration'] = $config;
  }
}

/**
 * Exception thrown when something goes wrong while accesing the Nuntium API.
 */
class NuntiumException extends Exception {

  /**
   * In case a channel creation or update failed, an array of key-value pairs
   * will be returned here with an explanation of which properties are invalid.
   *
   * @return array the invalid properties
   */
  function getProperties() {
    if (isset($this->properties)) {
      return $this->properties;
    } else {
      return array();
    }
  }

  function setProperties($properties) {
    $this->properties = $properties;
  }

}
?>
